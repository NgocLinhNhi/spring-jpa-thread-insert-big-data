package spring.boot.ember.js.io;

import java.io.File;

public interface FileFetcher {

    File getFile(String filePath);

}
