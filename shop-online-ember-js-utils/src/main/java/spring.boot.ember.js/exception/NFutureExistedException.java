package spring.boot.ember.js.exception;

import spring.boot.ember.js.concurrent.NFuture;

public class NFutureExistedException extends IllegalArgumentException {
    private static final long serialVersionUID = 4258315197030448654L;

    public NFutureExistedException(Object key, NFuture old) {
        super("future with key: " + key + " existed: " + old);
    }

}
