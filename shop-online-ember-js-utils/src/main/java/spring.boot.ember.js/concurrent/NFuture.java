package spring.boot.ember.js.concurrent;

public interface NFuture {

    void setResult(Object result);

    void setException(Exception exception);

    void cancel(String message);

    boolean isDone();

    <V> V get() throws Exception;

    <V> V get(long timeout) throws Exception;

}
