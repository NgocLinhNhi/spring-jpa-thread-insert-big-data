package spring.boot.ember.js.gateway.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import spring.boot.ember.js.components.entity.Category;
import spring.boot.ember.js.components.services.CategoryServices;

import java.util.List;

@RestController
@RequestMapping("/cate")
public class CategoryController {

    @Autowired
    CategoryServices categoryService;

    @GetMapping(value = "/loadAllCategory")
    public List<Category> loadAllCategory() throws Exception {
        return categoryService.loadAllCategory();
    }
}
