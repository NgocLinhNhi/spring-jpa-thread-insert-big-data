package spring.boot.ember.js.gateway.custom_exception;

public class ExcelFileSizeException extends RuntimeException {
    public ExcelFileSizeException(String message) {
        super(message);
    }
}
