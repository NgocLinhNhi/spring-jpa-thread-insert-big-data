package spring.boot.ember.js.gateway.validator;

import org.springframework.stereotype.Component;
import spring.boot.ember.js.components.request.CustomerRegisterRequest;
import spring.boot.ember.js.gateway.exception.GatewayBadRequestException;
import spring.boot.ember.js.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserValidator {

    public void validate(CustomerRegisterRequest user) {
        List<String> errors = new ArrayList<>();
        validatePhoneNumber(user.getPhone(), errors);
        validateAddress(user.getAddress(), errors);

        if (errors.size() > 0) throw new GatewayBadRequestException(errors);
    }

    private void validatePhoneNumber(String phoneNumber, List<String> errors) {
        if (StringUtil.isEmpty(phoneNumber)) {
            errors.add("Phone Number is required ");
        }
    }

    private void validateAddress(String address, List<String> errors) {
        if (StringUtil.isEmpty(address)) {
            errors.add("address is required ");
        }
    }
}
