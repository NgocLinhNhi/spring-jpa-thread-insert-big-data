package spring.boot.ember.js.gateway.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import spring.boot.ember.js.components.customer.CustomerServiceClient;
import spring.boot.ember.js.components.entity.User;
import spring.boot.ember.js.components.exception.CustomerDuplicateException;
import spring.boot.ember.js.components.exception.CustomerNotFoundException;
import spring.boot.ember.js.components.request.CustomerLoginRequest;
import spring.boot.ember.js.components.request.CustomerRegisterRequest;
import spring.boot.ember.js.gateway.exception.GatewayConflictException;
import spring.boot.ember.js.gateway.exception.GatewayRestrictedException;
import spring.boot.ember.js.gateway.validator.UserValidator;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private CustomerServiceClient customerServiceClient;

    @Autowired
    UserValidator userValidator;

    @PostMapping(value = "/login")
    public ModelAndView showWelcomePage(ModelMap model,
                                        @RequestParam String username,
                                        @RequestParam String password) throws Exception {
        ModelAndView mv;

        CustomerLoginRequest request = createCustomerLoginRequest(username, password);

        User loginUser;
        try {
            loginUser = customerServiceClient.call(request, User.class);
        } catch (CustomerNotFoundException e) {
            model.put("errorMessage", "Wrong User Name or password!!!");
            mv = new ModelAndView("user/login");
            return mv;
        }

        if (loginUser == null) {
            model.put("errorMessage", "Wrong User Name or password!!!");
            mv = new ModelAndView("user/login");
            return mv;
        }
        return new ModelAndView("pages/product");
    }

    //1 Cách khác trả về kết quả cho API
    //Khác với các API của product đây trả về 1 Object Json cho API
    @PostMapping(value = "/register")
    public Object registerMember(@RequestBody CustomerRegisterRequest request) {
        try {
            userValidator.validate(request);
            //Call BlockingQueue xử lý request theo queue
            return customerServiceClient.call(request, User.class);
        } catch (CustomerDuplicateException e) {
            throw new GatewayConflictException("Login Name :" + request.getLoginName() + " is existed");
        } catch (Exception e) {
            throw new GatewayRestrictedException();
        }
    }

    private CustomerLoginRequest createCustomerLoginRequest(String userName, String password) {
        CustomerLoginRequest request = new CustomerLoginRequest();
        request.setLoginName(userName);
        request.setPassword(password);
        return request;
    }
}
