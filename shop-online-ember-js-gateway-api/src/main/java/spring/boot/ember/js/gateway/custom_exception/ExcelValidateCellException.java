package spring.boot.ember.js.gateway.custom_exception;

public class ExcelValidateCellException extends RuntimeException {
    public ExcelValidateCellException(String message) {
        super(message);
    }
}
