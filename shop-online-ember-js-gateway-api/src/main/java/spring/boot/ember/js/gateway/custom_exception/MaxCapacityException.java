package spring.boot.ember.js.gateway.custom_exception;

public class MaxCapacityException extends RuntimeException {

    public MaxCapacityException(String msg) {
        super(msg);
    }

}
