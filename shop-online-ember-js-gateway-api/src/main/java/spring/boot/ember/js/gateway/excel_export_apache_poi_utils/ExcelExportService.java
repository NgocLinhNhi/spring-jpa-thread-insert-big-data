package spring.boot.ember.js.gateway.excel_export_apache_poi_utils;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import spring.boot.ember.js.components.mapper.ProductReportObject;
import spring.boot.ember.js.util.excel_apache_poi.SimpleExcelExportUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static spring.boot.ember.js.gateway.constant.Constant.FILENAME.*;

public class ExcelExportService {

    private SimpleExcelExportUtils simpleExcelExportUtils;
    private ExcelWriteContentFile excelWriteContentFile;

    public ExcelExportService() {
        simpleExcelExportUtils = new SimpleExcelExportUtils(EXCEL_EXPORT_RESULT_PATH, EXCEL_EXPORT_FILE_NAME);
        excelWriteContentFile = ExcelWriteContentFile.getInstance();
    }

    public void exportExcelFile(List<ProductReportObject> listData, HttpServletResponse response) throws IOException {
        XSSFWorkbook workbook = simpleExcelExportUtils.createXSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet(SHEET_NAME);
        simpleExcelExportUtils.writeHeaderLine(sheet, PRODUCT_HEADER);
        excelWriteContentFile.writeDataLines(listData, sheet);
        simpleExcelExportUtils.writeFile(response, workbook);
    }

}
