package spring.boot.ember.js.gateway.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import spring.boot.ember.js.components.view.ProductFileExcel;
import spring.boot.ember.js.gateway.custom_exception.ExcelFileSizeException;
import spring.boot.ember.js.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

@Component
public class ExcelValidator {
    private static final Logger logger = LoggerFactory.getLogger(ExcelValidator.class);

    public List<ProductFileExcel> listProductError = new ArrayList<>();
    private StringBuilder error;

    public void validate(List<ProductFileExcel> products) {
        validateFileSize(products);
        validateProducts(products);
    }

    public boolean test(List<ProductFileExcel> products) {
        for (ProductFileExcel pro : products) {
            if (pro.getCategory() == 1) {
                return true;
            }
        }
        return false;
    }

    private void validateFileSize(List<ProductFileExcel> products) {
        if (products.size() > 10000) {
            logger.error("File Dont Allow more than 10 Row !!!");
            throw new ExcelFileSizeException("PageSize dont allow more than 10 row");
        }
    }

    private void validateProducts(List<ProductFileExcel> products) {
        for (ProductFileExcel productFileExcel : products) {
            error = new StringBuilder();
            validateCategory(productFileExcel);
            validateProductName(productFileExcel);

            if (!StringUtil.isEmpty(error.toString())) {
                productFileExcel.setMessage(error.toString());
                listProductError.add(productFileExcel);
            }
        }
    }

    private void validateProductName(ProductFileExcel productFileExcel) {
        if (productFileExcel.getProductName().length() > 20) {
            error.append("ProductName cant > 20 character.");
        }
    }

    private void validateCategory(ProductFileExcel productFileExcel) {
        Long category = productFileExcel.getCategory();
        if (category > 100L) error.append("Category cant > 100. ");
    }
}
