package spring.boot.ember.js.gateway;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories("spring.boot.ember.js.components.repository")
//các module phải có đường dẫn package giống nhau thì springBoot mới autowired được
//coi chừng config scan project trong class PersistenceJPAConfig này nữa T_T
@ComponentScan({
        "spring.boot.ember.js.*" })
@EntityScan("spring.boot.ember.js.components.entity")
@SpringBootApplication()
public class GatewayApiStartUp extends SpringBootServletInitializer {

    public static void main(String[] args) throws Exception {
        GatewayBootstrap bootstrap = new GatewayBootstrap();
        bootstrap.start(GatewayApiStartUp.class);
    }

}
