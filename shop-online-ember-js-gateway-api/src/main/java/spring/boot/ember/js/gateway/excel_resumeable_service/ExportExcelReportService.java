package spring.boot.ember.js.gateway.excel_resumeable_service;

import org.jxls.common.Context;
import org.jxls.transform.Transformer;
import org.jxls.util.JxlsHelper;
import org.jxls.util.TransformerFactory;
import org.springframework.stereotype.Service;
import spring.boot.ember.js.components.mapper.ProductReportObject;
import spring.boot.ember.js.components.view.ProductFileExcel;
import spring.boot.ember.js.gateway.constant.Constant;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static spring.boot.ember.js.gateway.constant.Constant.ABSOLUTE_PATH;
import static spring.boot.ember.js.gateway.constant.Constant.FILENAME.CONTENT_REPORT;

@Service
public class ExportExcelReportService {

    public void exportReportProduct(HttpServletResponse response,
                                    Date startDate,
                                    Date endDate,
                                    List<ProductReportObject> rptList) throws IOException {
        InputStream is = createInputStream(Constant.FILENAME.PRODUCT_REPORT_EXCEL_TEMPLATE);
        String fileNameReport = createFileNameReport(Constant.FILENAME.PRODUCT_REPORT_EXCEL);
        Context context = createContext(startDate, endDate, rptList);
        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "inline; filename=\"" + fileNameReport + "\"");
        JxlsHelper.getInstance().processTemplate(is, response.getOutputStream(), context);
    }

    //Export file excel Error when import fail
    public void exportFileImportError(HttpServletResponse response,
                                      List<ProductFileExcel> listProductError) throws IOException {
        InputStream is = createInputStream(Constant.FILENAME.PRODUCT_IMPORT_ERROR_RESULT_TEMPLATE);
        String fileNameReport = createFileNameReport(Constant.FILENAME.PRODUCT_IMPORT_ERROR_RESULT);
        Context productContext = createProductContext(listProductError);
        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "inline; filename=\"" + fileNameReport + "\"");
        Transformer transformer = TransformerFactory.createTransformer(is, response.getOutputStream());
        JxlsHelper.getInstance().processTemplate(productContext, transformer);
    }

    private long getCurrentTimeCreateFile() {
        return new Date().getTime();
    }

    private String createFileNameReport(String fileName) {
        return fileName + getCurrentTimeCreateFile() + ".xlsx";
    }

    private InputStream createInputStream(String fileName) {
        return getClass().getResourceAsStream(ABSOLUTE_PATH + fileName);
    }

    private Context createProductContext(List<ProductFileExcel> listProductError) {
        Context context = new Context();
        List<ProductFileExcel> result = new ArrayList<>(listProductError);
        context.putVar("listProductError", result);
        return context;
    }

    private Context createContext(Date startDate, Date endDate, List<ProductReportObject> rptList) {
        Calendar c = Calendar.getInstance();
        Context context = new Context();
        c.setTime(startDate);
        c.setTime(endDate);
        context.putVar("rpTitle", CONTENT_REPORT);
        context.putVar("rpTitleYear", c.get(Calendar.YEAR));

        // rpMembers parm in file excel - cell
        context.putVar("rpMembers", rptList);
        return context;
    }


}