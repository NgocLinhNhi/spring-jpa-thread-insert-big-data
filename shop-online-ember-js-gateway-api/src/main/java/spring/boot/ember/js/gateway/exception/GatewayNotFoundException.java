package spring.boot.ember.js.gateway.exception;

public class GatewayNotFoundException extends RuntimeException {

    public GatewayNotFoundException(String message) { super(message);}
    public GatewayNotFoundException(String message, Exception e) {
        super(message, e);
    }

}
