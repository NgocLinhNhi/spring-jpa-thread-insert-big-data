package spring.boot.ember.js.gateway;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import spring.boot.ember.js.components.customer.CustomerRequestAnnotation;
import spring.boot.ember.js.components.customer.CustomerRequestHandler;
import spring.boot.ember.js.components.customer.CustomerRequestHandling;
import spring.boot.ember.js.components.customer.CustomerServiceClientLocal;

import java.util.Map;

public class GatewayBootstrap {
    private static final Logger logger = LoggerFactory.getLogger(GatewayBootstrap.class);

    private ApplicationContext appContext;

    public void start(Class<?> applicationClass) throws Exception {
        logger.info("START GATEWAY SERVICE !!!");
        runApp(applicationClass);
        postRun();
        logger.info("\nGATEWAY STARTED SUCCESSFULLY !!!\n");
    }

    private void runApp(Class<?> applicationClass) {
        appContext = SpringApplication.run(applicationClass);
    }

    private void postRun() throws Exception {
        startCustomerServiceClient();
    }

    private void startCustomerServiceClient() throws Exception {
        //Get Bean CustomerServiceClientLocal đã được khởi tạo bên class GatewayCustomerServiceConfig
        // ADD request-type vào trong BlockingQueue
        CustomerServiceClientLocal customerServiceClient = appContext.getBean(CustomerServiceClientLocal.class);
        Map<String, Object> customerRequestHandlers = appContext.getBeansWithAnnotation(CustomerRequestAnnotation.class);
        for (Object item : customerRequestHandlers.values()) {
            CustomerRequestHandler handler = (CustomerRequestHandler) item;
            String requestType = CustomerRequestHandling.getRequestType(handler);
            customerServiceClient.addRequestHandler(requestType, handler);
        }
        //Start = khởi tạo multithreading with executor cho Xử lý request trong blockingQueue
        customerServiceClient.start();
    }

}
