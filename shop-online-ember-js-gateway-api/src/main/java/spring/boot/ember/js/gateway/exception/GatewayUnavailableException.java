package spring.boot.ember.js.gateway.exception;

import lombok.Getter;

@Getter
public class GatewayUnavailableException extends RuntimeException {

    protected final int code;
    protected final String message;

    public GatewayUnavailableException(int code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }

    public String toJson() {
        return new StringBuilder()
                .append("{")
                .append("\"code\":").append(code).append(",")
                .append("\"message\":").append("\"" + message + "\"")
                .append("}")
                .toString();
    }
}
