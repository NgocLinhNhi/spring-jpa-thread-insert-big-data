package spring.boot.ember.js.gateway.config;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import spring.boot.ember.js.components.customer.CustomerServiceClient;
import spring.boot.ember.js.components.customer.CustomerServiceClientLocal;

@Setter
@Configuration
public class GatewayCustomerServiceConfig {

    @Value("${gateway.customer.service.max-capacity}")
    protected int maxCapacity;

    //Tạo Bean cho CustomerServiceClient và khởi tạo CustomerServiceClientLocal implements  CustomerServiceClient = bean too
    @Bean
    public CustomerServiceClient customerServiceClient() {
        return new CustomerServiceClientLocal(maxCapacity);
    }

}
