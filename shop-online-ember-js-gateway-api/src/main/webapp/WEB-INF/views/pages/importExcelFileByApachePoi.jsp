<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Thêm mới Product từ File Excel</title>

    <script src="/jquerry/jquery.min.js"></script>
    <script src="/jquerry/jquery.validate.min.js"></script>
    <!-- for validate form -->
    <script type="text/javascript" src="/jquerry/jquery.dataTables.min.js"></script>

    <!-- de bootstrap import dưới jquery ko bi conflict thu vien goi boostrap của jquerry ko gọi đc modal popup -->
    <link rel="stylesheet" type="text/css" href="/bootstrap/js/bootstrap.min.css">
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <!-- validate cho datatable data hien thi -->
    <script src="/DataTables/js/dataTables.bootstrap.min.js"></script>

    <script src="/js/commonAjax.js"></script>
    <link rel="stylesheet" type="text/css" href="/css/ErrorMessage.css">

    <!-- hỗ trợ download upload file = resumable -->
    <link rel="stylesheet" type="text/css" href="/resumable/css/style.css">
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Contain  -->
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="box col-md-12">

                    <div class="box-body">
                        <form method="POST"
                              id="singleUploadForm"
                              class="form-horizontal col-md-8 col-md-offset-2"
                              role="form">

                            <fieldset class="scheduler-border">
                                <legend class="scheduler-border">Thêm mới từ file Excel</legend>

                                <div class="col-md-12 form-group" style="float: left;">
                                    <div class="col-md-3 col-md-offset-2">Tải file mẫu</div>
                                    <div class="col-md-6">
                                        <a href="/download_template_import/TemplateImportProduct.xlsx" target="_self">
                                            TemplateImportProduct.xlsx</a>
                                    </div>
                                </div>

                                <div class="col-md-12 form-group" style="float: left;">
                                    <div class="col-md-3 col-md-offset-2">Đường dẫn file (Dung lượng tối đa 1MB)</div>

                                    <div class="col-md-6">
                                        <input id="singleFileUploadInput" type="file" name="file" class="file-input"
                                               required/>
                                    </div>
                                </div>

                                <div class="col-md-12 form-group"></div>
                                <div class="col-md-12 form-group">
                                    <div class="col-md-3 col-md-offset-5">
                                        <button type="submit" id="btnSubmit" class="primary submit-btn">Submit</button>
                                    </div>
                                </div>

                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->
    </div>
    <!-- end Contain -->

    <!-- Modal thông báo message-->
    <div class="modal fade" id="notifiModal" role="dialog">
        <form id="error-form"
              method="POST"
              action="/report/exportFileResultImport"
              role="form">
            <div class="modal-dialog">

                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close dongmodal" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Thông báo</h4>
                    </div>
                    <div class="modal-body">
                        <p id="massagemodal"></p>
                    </div>

                    <input type="hidden" id ="fileErrorName" name="fileErrorName" />

                    <div class="modal-footer">
                        <button id="exportFileErrBtn"
                                type="submit"
                                class="btn btn-primary">Export File Error
                        </button>

                        <button type="button"
                                class="btn btn-default dongmodal"
                                data-dismiss="modal">Đóng
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- endmodal -->

</div>

<script type="text/javascript">
    var fileNameExport;
    var singleUploadForm = document.querySelector('#singleUploadForm');
    var singleFileUploadInput = document.querySelector('#singleFileUploadInput');

    $(document).ready(function () {
        $("#btnSubmit").click(function (event) {
            event.preventDefault();
            validateFile();
            importFile();
        });
    });

    $('#error-form').submit(function () {
        document.getElementById("fileErrorName").value = fileNameExport;
        return true;
    });

    function validateFile() {
        var files = singleFileUploadInput.files;
        if (files.length === 0) {
            validateFileError("Please select a file");
        }

        if (files[0].size / 1024 > 1024) {
            validateFileError("Please choose a file < 1MB");
        }

        if (!files[0].name.endsWith(".xlsx")) {
            validateFileError("Please choose file excel");
        }
    }

    function importFile() {
        var form = $('#singleUploadForm')[0];
        var data = new FormData(form);

        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "/report/importExcelReportByApachePoi",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {
                importSuccess(data)
            },
            error: function (e) {
                importError(e)
            }
        });
    }


    function importSuccess(data) {
        var modal_header = $("#notifiModal .modal-header");
        var notify = $("#notifiModal");

        modal_header.removeClass("btn-danger");
        modal_header.addClass("btn-success");
        notify.addClass("fade");
        $('#massagemodal').html(data.message);
        $('#exportFileErrBtn').remove(); // remove button export file error if success
        notify.modal("toggle"); //hiển thị popup
        notify.on('hidden.bs.modal', function () {
            location.reload();
        });
    }

    function importError(data) {
        fileNameExport = null;
        var modal_header = $("#notifiModal .modal-header");
        var notify = $("#notifiModal");

        modal_header.removeClass("btn-success");
        modal_header.addClass("btn-danger");
        notify.addClass("fade");
        $('#massagemodal').html(data.responseJSON.message);//chuyển đổi message sang dạng Json của thư viện jquery
        fileNameExport = data.responseJSON.filePath;
        notify.modal("toggle");
        notify.on('hidden.bs.modal', function () {
            $(".modal-backdrop").remove();
            $("#notifiModal").removeAttr("style");
        });
    }

    function validateFileError(data) {
        var modal_header = $("#notifiModal .modal-header");
        var notify = $("#notifiModal");

        modal_header.removeClass("btn-success");
        modal_header.addClass("btn-danger");
        notify.addClass("fade");
        $('#massagemodal').html(data);
        notify.modal("toggle");
        notify.on('hidden.bs.modal', function () {
            $(".modal-backdrop").remove();
            $("#notifiModal").removeAttr("style");
        });
    }
</script>

</body>
</html>
