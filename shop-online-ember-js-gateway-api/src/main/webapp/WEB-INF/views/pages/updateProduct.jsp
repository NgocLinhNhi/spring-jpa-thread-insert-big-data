<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>

<head>
    <title>Update Product</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <script src="/jquerry/jquery.min.js"></script>
    <script src="/jquerry/jquery.validate.min.js"></script>
    <!-- for validate form -->
    <script type="text/javascript" src="/jquerry/jquery.dataTables.min.js"></script>

    <!-- de bootstrap import dưới jquery ko bi conflict thu vien goi boostrap của jquerry ko gọi đc modal popup -->
    <link rel="stylesheet" type="text/css" href="/bootstrap/js/bootstrap.min.css">
    <script src="/bootstrap/js/bootstrap.min.js"></script>

    <script src="/js/commonAjax.js"></script>
    <link rel="stylesheet" type="text/css" href="/css/ErrorMessage.css">

</head>
<body>
<div class="container" style="margin-top: 50px;">
    <div class="col-sm-10"></div>
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Update Product</h3>
            </div>

            <!--  -->
            <form class="form-horizontal" id="register-form" action="javascript:void(0);" method="POST">
                <div class="box-body">
                    <div class="form-group">
                        <label for="productName" class="col-sm-3 control-label">SEQ Product</label>
                        <div class="col-sm-9">
                            <input type="text"
                                   class="form-control"
                                   id="seqPro"
                                   name="seqPro"
                                   value='${product.seqPro}'
                                   readonly="readonly"
                                   placeholder="Mã sản phẩm">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Category</label>
                        <div class="col-md-9">
                            <select id="categoryId" name="categoryId" class="form-control select2">
                                <option value='${product.categoryId}'
                                        selected="selected">${product.categoryName}</option>
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="productName" class="col-sm-3 control-label">Product Name</label>
                        <div class="col-sm-9">
                            <input type="text"
                                   class="form-control"
                                   id="productName"
                                   name="productName"
                                   value='${product.productName}'
                                   placeholder="Tên sản phẩm">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="price" class="col-sm-3 control-label">Price</label>
                        <div class="col-sm-9">
                            <input type="number"
                                   class="form-control"
                                   id="price"
                                   name="price"
                                   value='${product.price}'
                                   placeholder="Giá cả">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="imageProduct" class="col-sm-3 control-label">Image</label>
                        <div class="col-sm-9">
                            <input type="number"
                                   class="form-control"
                                   id="imageProduct"
                                   name="imageProduct"
                                   value='${product.imageProduct}'
                                   placeholder="URL">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="numberSales" class="col-lg-3 control-label">number sales</label>
                        <div class="col-lg-9">
                            <input type="number"
                                   class="form-control"
                                   id="numberSales"
                                   name="numberSales"
                                   value='${product.numberSales}'
                                   placeholder="Số lượng">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="guarantee" class="col-sm-3 control-label">Guarantee</label>
                        <div class="col-sm-9">
                            <input type="number"
                                   class="form-control"
                                   id="guarantee"
                                   name="guarantee"
                                   value='${product.guarantee}'
                                   placeholder="Bảo hành">
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="sysStatus" class="col-sm-3 control-label">sysStatus</label>
                        <div class="col-sm-9">
                            <input type="number"
                                   class="form-control"
                                   id="sysStatus"
                                   name="sysStatus"
                                   value='${product.sysStatus}'
                                   placeholder="Tình trạng">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="updateDate" class="col-sm-3 control-label">Update Date</label>
                        <div class="col-sm-9">
                            <input type="text"
                                   class="form-control"
                                   id="updateDate"
                                   name="updateDate"
                                   value='${product.updateDate}'
                                   readonly="readonly"
                                   placeholder="Ngày Update">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="createDate" class="col-sm-3 control-label">Creat Date</label>
                        <div class="col-sm-9">
                            <input type="text"
                                   class="form-control"
                                   id="createDate"
                                   name="createDate"
                                   value='${product.createDate}'
                                   readonly="readonly"
                                   placeholder="Ngày tạo">
                        </div>
                    </div>


                </div>
                <div class="box-footer">
                    <button type="submit" name="save" id="btn-save" class="btn btn-primary pull-right">Update</button>
                </div>
            </form>
        </div>
    </div>
    <div class="col-md-3"></div>

    <!-- Modal thông báo message add-->
    <div class="modal fade" id="notifiModal" role="dialog">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close dongmodal" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Thông báo</h4>
                </div>
                <div class="modal-body">
                    <p id="massagemodal"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default dongmodal"
                            data-dismiss="modal">Đóng
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- endmodal -->
</div>

<script type="text/javascript">
    CommonAjax.get(null, "/cate/loadAllCategory", getInfoSuccess, null);

    function getInfoSuccess(data) {
        var categoryInfo = data;
        for (var i = 0; i < categoryInfo.length; i++) {
            var cate = categoryInfo[i];
            $("#categoryId").append('<option value="' + cate.categoryId + '">' + cate.categoryName + '</option>')
        }
    }

    //validate form
    var formValidator = $("#register-form").validate({
        rules: {
            productName: {
                required: true,
                maxlength: 255
            },
            price: {
                required: true
            },
            imageProduct: {
                required: true,
                maxlength: 255
            },
            numberSales: {
                required: true
            },
            guarantee: {
                required: true
            },
            sysStatus: {
                required: true
            }
        },
        messages: {
            productName: {
                required: "Vui lòng nhập họ tên",
                maxlength: "Độ dài product Name không vượt quá 255 kí tự"
            },
            price: {
                required: "Vui lòng nhập vào price"
            },
            imageProduct: {
                required: "Vui lòng nhập image",
                maxlength: "Độ dài image không vượt quá 255 kí tự"
            },
            numberSales: {
                required: "Vui lòng nhập địa chỉ số lượng"
            },
            guarantee: {
                required: "Vui lòng nhập số tháng bảo hành"
            },
            sysStatus: {
                required: "Vui lòng nhập tình trạng"
            }
        }
    });

    $("#btn-save").click(function () {
        formValidator.settings.submitHandler = updateProduct;
    });

    function updateProduct() {
        var seqProInput = $("#seqPro").val().trim();
        var productNameInput = $("#productName").val().trim();
        var priceInput = $('#price').val().trim();
        var imageProductInput = $('#imageProduct').val().trim();
        var numberSalesInput = $("#numberSales").val().trim();
        var guaranteeInput = $("#guarantee").val().trim();
        var sysStatusInput = $("#sysStatus").val().trim();
        var createDateInput = $("#createDate").val().trim();
        var categoryIdInput = $('#categoryId').val().trim();

        var seqPro = seqProInput !== "" ? seqProInput : null;
        var productName = productNameInput !== "" ? productNameInput : null;
        var price = priceInput !== "" ? priceInput : null;
        var imageProduct = imageProductInput !== "" ? imageProductInput : null;
        var numberSales = numberSalesInput !== "" ? numberSalesInput : null;
        var guarantee = guaranteeInput !== "" ? guaranteeInput : null;
        var sysStatus = sysStatusInput !== "" ? sysStatusInput : null;
        var createDate = createDateInput !== "" ? createDateInput : null;
        var categoryId = categoryIdInput !== "" ? categoryIdInput : null;

        var updateDate = new Date();

        var memberInfo = {
            "seqPro": seqPro,
            "productName": productName,
            "price": price,
            "imageProduct": imageProduct,
            "numberSales": numberSales,
            "guarantee": guarantee,
            "sysStatus": sysStatus,
            "createDate": createDate,
            "updateDate": updateDate,
            "category": {
                "categoryId": categoryId
            }
        };
        CommonAjax.post(memberInfo, "/product/updateProduct", updateSuccess, updateError);
    }

    function updateSuccess(data) {
        var modal_header = $("#notifiModal .modal-header");
        var notify = $("#notifiModal");

        modal_header.removeClass("btn-danger");
        modal_header.addClass("btn-success");
        notify.addClass("fade");
        $('#massagemodal').html(data.message);//add message vao message của popup
        notify.modal("toggle"); //hiển thị popup
        notify.on('hidden.bs.modal', function () {
            location.reload();
        });
    }

    function updateError(data) {
        var modal_header = $("#notifiModal .modal-header");
        var notify = $("#notifiModal");

        modal_header.removeClass("btn-success");
        modal_header.addClass("btn-danger");
        notify.addClass("fade");
        $('#massagemodal').html(data.responseJSON.message);//chuyển đổi message sang dạng Json của thư viện jquery
        notify.modal("toggle");
        notify.on('hidden.bs.modal', function () {
            $(".modal-backdrop").remove();
            $("#notifiModal").removeAttr("style");
        });
    }
</script>
</body>
</html>