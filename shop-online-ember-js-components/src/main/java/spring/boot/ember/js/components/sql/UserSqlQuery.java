package spring.boot.ember.js.components.sql;

public class UserSqlQuery {
    public static final String LOGIN_SPRING_JPA = "select us from User us where us.loginName = ?1 and us.password = ?2 and isDelete=1";
    public static final String SELECT_MAX_SEQ_JPA = "select max(seqUser) from User";
    public static final String CHECK_EXIST_USER_NAME_JPA = "select us from User us where us.loginName = ?1 and isDelete=1";

}
