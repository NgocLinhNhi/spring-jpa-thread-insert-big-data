package spring.boot.ember.js.components.batch.handle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spring.boot.ember.js.components.entity.Product;
import spring.boot.ember.js.components.repository.ProductRepository;

import java.util.List;

public class ProductCallableHandler {
    private final Logger logger = LoggerFactory.getLogger(ProductExecuteCallable.class);

    public static ProductCallableHandler getInstance() {
        return new ProductCallableHandler();
    }

    String handlerProductCallable(List<Product> lst, ProductRepository productRepository) throws Exception {
        logger.info("List Product insert after partition: " + lst.size());
        productRepository.saveAll(lst);
        return "SUCCESS";
    }
}
