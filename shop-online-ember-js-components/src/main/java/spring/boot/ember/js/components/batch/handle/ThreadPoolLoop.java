package spring.boot.ember.js.components.batch.handle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPoolLoop {

    private final Logger logger = LoggerFactory.getLogger(ThreadPoolLoop.class);

    private static ThreadPoolLoop INSTANCE = new ThreadPoolLoop();

    public static ThreadPoolLoop getInstance() {
        return INSTANCE;
    }

    public ExecutorService newExecutorService(int threadPoolSize) {
        logger.info("Thread number : " + threadPoolSize);
        return Executors.newFixedThreadPool(threadPoolSize);
    }

}
