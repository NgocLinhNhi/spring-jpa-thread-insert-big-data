package spring.boot.ember.js.components.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import spring.boot.ember.js.components.entity.User;

import static spring.boot.ember.js.components.sql.UserSqlQuery.*;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query(LOGIN_SPRING_JPA)
    User loginUser(String loginName, String password);

    @Query(SELECT_MAX_SEQ_JPA)
    long selectMaxSeqUser();

    @Query(CHECK_EXIST_USER_NAME_JPA)
    User checkExistUser(String loginName);

}
