package spring.boot.ember.js.components.customer;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spring.boot.ember.js.components.entity.User;
import spring.boot.ember.js.components.exception.CustomerNotFoundException;
import spring.boot.ember.js.components.request.CustomerLoginRequest;
import spring.boot.ember.js.components.request.CustomerRequestTypes;
import spring.boot.ember.js.components.services.CustomerServices;

@Setter
@Component
@CustomerRequestAnnotation(CustomerRequestTypes.GET)
public class CustomerGetInfoRequestHandler extends CustomerRequestAbstractHandler<CustomerLoginRequest> {

    @Autowired
    CustomerServices customerService;

    @Override
    public Object handle(CustomerLoginRequest request) {
        User checkLogin = customerService.login(createUser(request));
        if (checkLogin == null)
            throw new CustomerNotFoundException("User Login Name: " + request.getLoginName() + " not existed");

        return checkLogin;
    }

    private User createUser(CustomerLoginRequest request) {
        User user = new User();
        user.setLoginName(request.getLoginName());
        user.setPassword(request.getPassword());
        return user;
    }
}
