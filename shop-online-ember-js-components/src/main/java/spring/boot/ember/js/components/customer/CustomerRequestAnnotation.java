package spring.boot.ember.js.components.customer;

import java.lang.annotation.*;

@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE })
public @interface CustomerRequestAnnotation {

    //Tạo 1 annotation @
    String value();

}
