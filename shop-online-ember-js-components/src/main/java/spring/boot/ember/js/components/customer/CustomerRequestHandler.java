package spring.boot.ember.js.components.customer;

import spring.boot.ember.js.components.request.CustomerRequest;

public interface CustomerRequestHandler<R extends CustomerRequest> {

    Object handle(R request) throws Exception;

}
