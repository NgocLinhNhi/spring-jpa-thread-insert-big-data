package spring.boot.ember.js.components.exception;

public class MaxCapacityException extends RuntimeException {

    public MaxCapacityException(String msg) {
        super(msg);
    }

}
