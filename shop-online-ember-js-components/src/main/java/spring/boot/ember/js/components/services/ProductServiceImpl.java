package spring.boot.ember.js.components.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.boot.ember.js.components.entity.Product;
import spring.boot.ember.js.components.exception.ProductNotFoundException;
import spring.boot.ember.js.components.mapper.ProductMapper;
import spring.boot.ember.js.components.repository.ProductRepository;

import java.util.Date;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductRepository productRepository;

    @Override
    public List<Product> loadAllProduct() {
        List<Product> lstProducts = productRepository.loadAllProduct();
        if (lstProducts == null) {
            throw new ProductNotFoundException("Not Found Product");
        }
        List<Product> products = ProductMapper.getInstance().lstProductMapper(lstProducts);
        return products;
    }

    @Override
    public Product findProductById(Long seqPro) throws Exception {
        Product result = productRepository.findProductById(seqPro);
        if (result == null) {
            throw new ProductNotFoundException("Not Found Product");
        }
        return ProductMapper.getInstance().productMapper(result);
    }

    @Override
    public void addNewProduct(Product pro) throws Exception {
        createProduct(pro);
        productRepository.save(pro);
    }

    @Override
    public void deleteProduct(Long seqPro) {
        productRepository.deleteById(seqPro);
    }

    @Override
    public void updateProduct(Product pro) {
        productRepository.save(pro);
    }


    // Create data insert
    private void createProduct(Product pro) throws Exception {
        long selectMaxSeqProduct = productRepository.selectMaxSeqProduct();
        long seqNo = selectMaxSeqProduct + 1;

        pro.setSeqPro(seqNo);
        pro.setSysStatus(new Long("1"));
        pro.setIsUsing("N");

        Date date = new Date();
        pro.setCreateDate(date);
        pro.setUpdateDate(date);
    }
}
