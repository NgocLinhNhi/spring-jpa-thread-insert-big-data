package spring.boot.ember.js.components.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.boot.ember.js.components.entity.Category;
import spring.boot.ember.js.components.mapper.CategoryMapper;
import spring.boot.ember.js.components.repository.CategoryRepository;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryServices {

    @Autowired
    CategoryRepository categoryRepository;

    @Override
    public List<Category> loadAllCategory() {
        System.out.println("Total category: " + categoryRepository.loadAllCategory().size());
        return CategoryMapper.getInstance().categoryMapper(categoryRepository.loadAllCategory());
    }
}
