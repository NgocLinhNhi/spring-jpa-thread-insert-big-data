package spring.boot.ember.js.components.sql;

public class ProductSqlQuery {
    public static final String MAX_SEQ_PRODUCT = "select max(seqPro) from Product";
    public static final String LOAD_ALL_PRODUCT = "SELECT * from Product pro";
    public static final String FIND_PRODUCT_BY_SEQ = "SELECT * from Product pro where pro.seq_Pro =?";
    public static final String INSERT_PRODUCT = "INSERT INTO PRODUCT (SEQ_PRO, PRODUCT_NAME, CATEGORY_ID) VALUES(?1,?2,?3) ";


    public static StringBuilder insertProductQuery() {
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO PRODUCT                       ")
                .append("    (SEQ_PRO,                        ")
                .append("    PRODUCT_NAME,                    ")
                .append("    CATEGORY_ID,                     ")
                .append("    GUARANTEE,                       ")
                .append("    NUMBER_SALES,                    ")
                .append("    PRICE,                           ")
                .append("    IMAGE_URL,                       ")
                .append("    IS_USING,                        ")
                .append("    SYS_STATUS,                      ")
                .append("    CREATE_DATE,                     ")
                .append("    UPDATE_DATE)                     ")
                .append("    VALUES(?,?,?,?,?,?,?,?,?,?,?)    ");
        return sql;
    }
}
