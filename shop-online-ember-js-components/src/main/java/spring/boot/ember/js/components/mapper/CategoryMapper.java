package spring.boot.ember.js.components.mapper;

import spring.boot.ember.js.components.entity.Category;

import java.util.ArrayList;
import java.util.List;

public class CategoryMapper {

    private static CategoryMapper INSTANCE;

    public static CategoryMapper getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new CategoryMapper();
        }
        return INSTANCE;
    }

    public List<Category> categoryMapper(List<Category> lstData) {
        List<Category> result = new ArrayList<>();
        for (Category cate : lstData) {
            Category category = new Category();
            category.setCategoryId(cate.getCategoryId());
            category.setCategoryName(cate.getCategoryName());

            result.add(category);
        }
        return result;
    }
}
