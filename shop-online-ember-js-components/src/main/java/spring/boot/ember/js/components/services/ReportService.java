package spring.boot.ember.js.components.services;

import spring.boot.ember.js.components.mapper.ProductReportObject;
import spring.boot.ember.js.components.view.ProductFileExcel;

import java.util.List;

public interface ReportService {
    List<ProductReportObject> exportProductReport() throws Exception;

    void insertProductReportByBatch(List<ProductFileExcel> listProduct) throws Exception;
}
