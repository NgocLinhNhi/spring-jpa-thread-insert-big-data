package spring.boot.ember.js.components.services;

import spring.boot.ember.js.components.entity.User;

public interface CustomerServices {

    User login(User user);

    void registerMember(User us) throws Exception;

    long selectMaxSeq() throws Exception;

    User checkExistLoginName(String loginName) throws Exception;

}
