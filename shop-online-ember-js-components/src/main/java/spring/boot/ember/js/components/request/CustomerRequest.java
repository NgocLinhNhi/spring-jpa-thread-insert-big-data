package spring.boot.ember.js.components.request;

public interface CustomerRequest {

    String getLoginName();

    String getRequestType();

}
