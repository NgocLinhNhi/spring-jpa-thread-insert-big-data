package spring.boot.ember.js.components.request;

public final class CustomerRequestTypes {

    public static final String REGISTER = "register";
    public static final String UPDATE = "update";
    public static final String GET = "get";

    private CustomerRequestTypes() {}

}
