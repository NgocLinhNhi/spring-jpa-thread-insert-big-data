package spring.boot.ember.js.components.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.boot.ember.js.components.entity.User;
import spring.boot.ember.js.components.repository.UserRepository;

@Service
public class CustomerServiceImpl implements CustomerServices {

    private static final Logger logger = LoggerFactory.getLogger(CustomerServiceImpl.class);

    @Autowired
    UserRepository userRepository;

    @Override
    public User login(User user) {
        logger.info("Login Process with Spring-JPA");
        return userRepository.loginUser(user.getLoginName(), user.getPassword());
    }

    @Override
    public void registerMember(User user) {
        logger.info("Register Process with Spring-JPA");
        userRepository.save(user);
    }

    @Override
    public long selectMaxSeq() {
        return userRepository.selectMaxSeqUser();
    }

    @Override
    public User checkExistLoginName(String loginName) {
        return userRepository.checkExistUser(loginName);
    }

}