package spring.boot.ember.js.components.batch.handle;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExecutorConfig {
    public static int threadPoolSize = 10;
    public static int threadPoolSizeLv2 = 20;
}
