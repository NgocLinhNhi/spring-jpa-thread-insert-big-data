package spring.boot.ember.js.components.batch.handle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spring.boot.ember.js.components.entity.Product;
import spring.boot.ember.js.components.repository.ProductRepository;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class ProductExecuteCallable {
    private final Logger logger = LoggerFactory.getLogger(ProductExecuteCallable.class);

    private Collection<Future<?>> futures = new LinkedList<>();

    public static ProductExecuteCallable INSTANCE;

    public static ProductExecuteCallable getInstance() {
        if (INSTANCE == null) INSTANCE = new ProductExecuteCallable();
        return INSTANCE;
    }

    public void start(List<Product> listData,
                      int partitionSize,
                      ProductRepository productRepository) throws Exception {
        processHandleVietLottCallable(listData, partitionSize, productRepository);
    }


    private void processHandleVietLottCallable(List<Product> listData,
                                               int batchSize,
                                               ProductRepository productRepository) throws Exception {
        //phải tạo mới lại ExecutorService cho request sau
        //vì nếu shutdown executor sẽ bị java.util.concurrent.RejectedExecutionException ngay
        //executorService.shutdown();
        ExecutorService executorService = ThreadPoolLoop.getInstance().newExecutorService(ExecutorConfig.threadPoolSize);
        List<List<Product>> partition = partitionData(listData, batchSize);
        executeFuture(partition, executorService, productRepository);
    }


    private List<List<Product>> partitionData(List<Product> listData, int batchSize) {
        return MyPartition.partition(listData, batchSize);
    }

    private void executeFuture(List<List<Product>> partition,
                               ExecutorService executorService,
                               ProductRepository productRepository) throws InterruptedException, ExecutionException {
        for (List<Product> lst : partition) {
            ProductCallable vietLottCallable = new ProductCallable(lst, productRepository);
            Future<?> future = executorService.submit(vietLottCallable);
            futures.add(future);
        }

        for (Future<?> future : futures) {
            System.out.println("wait Thread");
            future.get();
        }

        executorService.shutdown();
        boolean finished = executorService.awaitTermination(1, TimeUnit.SECONDS);
        if (finished) logger.info("Finish generation");
        else logger.error("Generate report take more than one day. Stop");
    }
}
