package spring.boot.ember.js.components.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import spring.boot.ember.js.components.entity.Product;

import java.util.List;

import static spring.boot.ember.js.components.sql.ProductSqlQuery.*;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    @Query(value = LOAD_ALL_PRODUCT,
            nativeQuery = true)
    List<Product> loadAllProduct();

    @Query(value = FIND_PRODUCT_BY_SEQ,
            nativeQuery = true)
    Product findProductById(Long seqPro) throws Exception;

    @Query(value = MAX_SEQ_PRODUCT)
    long selectMaxSeqProduct() throws Exception;

}
