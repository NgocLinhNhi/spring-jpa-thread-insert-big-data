package spring.boot.ember.js.components.services;

import spring.boot.ember.js.components.entity.Category;

import java.util.List;

public interface CategoryServices {
    List<Category> loadAllCategory() throws Exception;
}
