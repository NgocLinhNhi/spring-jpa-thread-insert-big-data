package spring.boot.ember.js.components.batch.handle;

import lombok.Getter;
import lombok.Setter;
import spring.boot.ember.js.components.entity.Product;
import spring.boot.ember.js.components.repository.ProductRepository;

import java.util.List;
import java.util.concurrent.Callable;

@Getter
@Setter
public class ProductCallable implements Callable<String> {

    //test ahihi 123
    private List<Product> listData;
    ProductRepository productRepository;


    ProductCallable(List<Product> listData, ProductRepository productRepository) {
        this.listData = listData;
        this.productRepository = productRepository;
    }

    @Override
    public String call() throws Exception {
        return handlerProduct(this.listData, this.productRepository);
    }

    private String handlerProduct(List<Product> listData, ProductRepository productRepository) throws Exception {
        return ProductCallableHandler.getInstance().handlerProductCallable(listData, productRepository);
    }
}
