package spring.boot.ember.js.components.exception;

public class CustomerDuplicateException extends IllegalArgumentException {

    public CustomerDuplicateException(String message) {
        super(message);
    }

}
