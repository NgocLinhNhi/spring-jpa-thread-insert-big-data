package spring.boot.ember.js.components.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import spring.boot.ember.js.components.entity.Category;

import java.util.List;

import static spring.boot.ember.js.components.sql.CategorySqlQuery.GET_ALL_CATEGORY;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

    @Query(value = GET_ALL_CATEGORY, nativeQuery = true)
    List<Category> loadAllCategory();
}
