package spring.boot.ember.js.components.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.boot.ember.js.components.batch.handle.ProductExecuteCallable;
import spring.boot.ember.js.components.entity.Category;
import spring.boot.ember.js.components.entity.Product;
import spring.boot.ember.js.components.mapper.ProductReportObject;
import spring.boot.ember.js.components.mapper.ReportMapper;
import spring.boot.ember.js.components.repository.ProductRepository;
import spring.boot.ember.js.components.view.ProductFileExcel;

import java.util.ArrayList;
import java.util.List;

@Service
public class ReportServicesImpl implements ReportService {
    @Autowired
    ProductRepository productRepository;

    @Override
    public List<ProductReportObject> exportProductReport() {
        List<Product> products = productRepository.loadAllProduct();
        return ReportMapper.getInstance().reportMapRowData(products);
    }

    //sử dụng multithreading để xử lý update big data + spring JPA sử dụng insert batch theo size
    @Override
    public void insertProductReportByBatch(List<ProductFileExcel> listProduct) throws Exception {
        List<Product> products = createProducts(listProduct);
        ProductExecuteCallable.getInstance().start(products, 26, productRepository);
    }

    private List<Product> createProducts(List<ProductFileExcel> listProduct) throws Exception {
        List<Product> listProducts = new ArrayList<>();

        long seqNo = getMaxSeqPro();
        Product product;

        for (ProductFileExcel pro : listProduct) {
            product = new Product();
            Category cate = new Category();

            product.setSeqPro(seqNo);
            cate.setCategoryId(pro.getCategory());
            product.setCategory(cate);
            product.setProductName(pro.getProductName());

            listProducts.add(product);
            seqNo++;
        }
        return listProducts;
    }

    private long getMaxSeqPro() throws Exception {
        long selectMaxSeqProduct = productRepository.selectMaxSeqProduct();
        return selectMaxSeqProduct + 1;
    }
}
