package spring.boot.ember.js.components.mapper;

import spring.boot.ember.js.components.entity.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductMapper {

    private static ProductMapper INSTANCE;

    public static ProductMapper getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ProductMapper();
        }
        return INSTANCE;
    }

    //Xào nấu data from DB select
    public List<Product> lstProductMapper(List<Product> lstProduct) {
        List<Product> result = new ArrayList<>();
        for (Product pro : lstProduct) {
            Product product = new Product();

            product.setSeqPro(pro.getSeqPro());
            product.setCategoryName(pro.getCategory().getCategoryName());
            product.setCategoryId(pro.getCategory().getCategoryId());
            product.setCreateDate(pro.getCreateDate());
            product.setUpdateDate(pro.getUpdateDate());
            product.setIsUsing(pro.getIsUsing());
            product.setProductName(pro.getProductName());
            product.setSysStatus(pro.getSysStatus());
            product.setGuarantee(pro.getGuarantee());
            product.setNumberSales(pro.getNumberSales());
            product.setPrice(pro.getPrice());
            product.setImageProduct(pro.getImageProduct());

            result.add(product);
        }
        return result;
    }

    public Product productMapper(Product productResult) {
        Product product = new Product();

        product.setSeqPro(productResult.getSeqPro());
        product.setCategoryName(productResult.getCategory().getCategoryName());
        product.setCategoryId(productResult.getCategory().getCategoryId());
        product.setIsUsing(productResult.getIsUsing());
        product.setCreateDate(productResult.getCreateDate());
        product.setUpdateDate(productResult.getUpdateDate());
        product.setProductName(productResult.getProductName());
        product.setSysStatus(productResult.getSysStatus());
        product.setGuarantee(productResult.getGuarantee());
        product.setNumberSales(productResult.getNumberSales());
        product.setPrice(productResult.getPrice());
        product.setImageProduct(productResult.getImageProduct());

        return product;
    }
}
