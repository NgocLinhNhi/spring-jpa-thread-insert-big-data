package spring.boot.ember.js.components.mapper;

import spring.boot.ember.js.components.entity.Product;
import spring.boot.ember.js.util.DateUtil;

import java.util.ArrayList;
import java.util.List;

import static spring.boot.ember.js.util.DateConstant.PATTERN_YY_MM_DD;

public class ReportMapper {

    private static ReportMapper INSTANCE;

    public static ReportMapper getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ReportMapper();
        }
        return INSTANCE;
    }

    public List<ProductReportObject> reportMapRowData(List<Product> lstProduct) {
        List<ProductReportObject> result = new ArrayList<>();
        for (Product pro : lstProduct) {
            ProductReportObject reportObject = new ProductReportObject();

            reportObject.setSeqPro(pro.getSeqPro());
            reportObject.setCategoryName(pro.getCategory().getCategoryName());
            reportObject.setCategoryId(pro.getCategory().getCategoryId());
            reportObject.setCreateDate(DateUtil.toString(pro.getCreateDate(), PATTERN_YY_MM_DD));
            reportObject.setUpdateDate(DateUtil.toString(pro.getUpdateDate(), PATTERN_YY_MM_DD));
            reportObject.setPrice(pro.getPrice());
            reportObject.setProductName(pro.getProductName());
            reportObject.setSysStatus(pro.getSysStatus());
            reportObject.setGuarantee(pro.getGuarantee());
            reportObject.setNumberSales(pro.getNumberSales());
            reportObject.setImageProduct(pro.getImageProduct());

            result.add(reportObject);
        }
        return result;
    }

}