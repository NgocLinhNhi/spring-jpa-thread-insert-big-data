package spring.boot.ember.js.components.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerLoginRequest implements CustomerRequest {

    protected String loginName;
    private String password;

    @Override
    public String getRequestType() {
        return CustomerRequestTypes.GET;
    }
}
