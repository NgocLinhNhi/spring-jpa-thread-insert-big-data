package spring.boot.ember.js.components.customer;

import spring.boot.ember.js.components.request.CustomerRequest;

public interface CustomerServiceClient {

    <T> T call(CustomerRequest request, Class<T> outType) throws Exception;

}
